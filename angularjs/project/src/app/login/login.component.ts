import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
// loginform=this.fb.group({
//   username:[''],
//   password:['']

// })
loginform=new FormGroup({
  username: new FormControl('',[Validators.required,Validators.email]),
  password: new FormControl('',Validators.required)
})

submitted=false;
constructor(
  private fb:FormBuilder
){}

  ngOnInit(): void { }

get f(){
  
   return this.loginform.controls

}


  loginFormSubmit(){
    this.submitted=true;

    if(this.loginform.invalid){
      
      //console.log(this.loginform.value);
      return;
    }else{
    console.log(this.loginform.value);
    }
  }

}
